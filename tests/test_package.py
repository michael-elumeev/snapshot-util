import subprocess


def test_install():
    subprocess.check_call(["pip", "install", '.'])
    subprocess.check_call(["snapshot", "--help"])
    subprocess.check_call(["pip", "uninstall", "-y", "snapshot"])


def test_readme():
    with open("README.md") as f:
        assert "snapshot" in f.read(),\
            "Please add README.md and provide description of snapshot util"
