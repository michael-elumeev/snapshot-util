from setuptools import setup, find_packages
setup(
    name="snapshot",
    packages=find_packages(),
    entry_points={
        "console_scripts": [
            "snapshot = snapshot.snapshot:main",
        ],
    },
    install_requires=[
        "psutil",
    ],
    version="0.1",
    author="Michael ELumeev",
    author_email="captain_jack@gmail.com",
    description="Snapshot utility for your system",
    license="MIT")
