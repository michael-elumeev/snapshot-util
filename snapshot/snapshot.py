import psutil
import argparse
import time
import os
import json

class SystemInfo():
    def __init__(self):
        self.kib_mem = "KiB Mem"
        self.kib_swap = "KiB Swap"
        self.answer = {"Tasks": {}, "%CPU": {}, "KiB Mem": {}, "KiB Swap": {}}
        self.pids_parser()
        self.cpu_parser()
        self.mem_parser()
        self.set_timestamp()
        self.json_answer = json.dumps(self.answer)

    def cpu_parser(self):
        cpu_info = psutil.cpu_times_percent(interval=1, percpu=False)
        self.answer["%CPU"]["user"] = cpu_info.user
        self.answer["%CPU"]["system"] = cpu_info.system
        self.answer["%CPU"]["idle"] = cpu_info.idle


    def pids_parser(self):
        pids = psutil.pids()
        self.answer["Tasks"]["total"] = 0
        self.answer["Tasks"]["running"] = 0
        self.answer["Tasks"]["sleeping"] = 0
        self.answer["Tasks"]["stopped"] = 0
        self.answer["Tasks"]["zombie"] = 0
        for pid in pids:
            try:
                p = psutil.Process(pid)
                self.answer["Tasks"]["total"] += 1
                if p.status() == "running":
                    self.answer["Tasks"]["running"] += 1
                elif p.status() == "stopped":
                    self.answer["Tasks"]["stopped"] += 1
                elif p.status() == "sleeping":
                    self.answer["Tasks"]["sleeping"] += 1
                elif p.status() == "zombie":
                    self.answer["Tasks"]["zombie"] += 1
            except:
                print("oops")


    def mem_parser(self):
        mem = psutil.virtual_memory()
        swap = psutil.swap_memory()
        self.answer[self.kib_mem]["total"] = round(mem.total / 1024)
        self.answer[self.kib_mem]["free"] = round(mem.free / 1024)
        self.answer[self.kib_mem]["used"] = round(mem.used / 1024)
        self.answer[self.kib_swap]["total"] = round(swap.total / 1024)
        self.answer[self.kib_swap]["free"] = round(swap.free / 1024)
        self.answer[self.kib_swap]["used"] = round(swap.used / 1024)

    def set_timestamp(self):
        ts = int(time.time())
        self.answer["Timestamp"] = ts

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", help="Interval between snapshots in seconds", type=int, default=30)
    parser.add_argument("-f", help="Output file name", default="snapshot.json")
    parser.add_argument("-n", help="Quantity of snapshot to output", type=int, default=20)
    args = parser.parse_args()
    time_interval = args.i
    snapshots = args.n
    filename = args.f
    try:
        os.remove(filename)
    except:
        print("oops")
    with open(filename, "a") as file:
        for num in range(snapshots):
            system_info = SystemInfo()
            os.system('clear')
            # print(system_info.json_answer)
            print(system_info.answer, end='\r')
            file.write(system_info.json_answer)
            file.write("\n")
            time.sleep(time_interval)

if __name__ == "__main__":
    main()
